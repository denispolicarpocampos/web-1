class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.decimal :item_price, null: false, precision: 15, scale: 2
      t.string :purchaser_name, null: false
      t.text :item_description, null: false
      t.integer :purchase_count, null: false
      t.string :merchant_address, null: false
      t.string :merchant_name, null: false
      t.references :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
