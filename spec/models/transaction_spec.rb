require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:item_price) }
    it { should validate_presence_of(:purchaser_name) }
    it { should validate_presence_of(:item_description) }
    it { should validate_presence_of(:purchase_count) }
    it { should validate_presence_of(:merchant_address) }
    it { should validate_presence_of(:merchant_name) }

    it { should validate_presence_of(:user_id) }
    it { should validate_numericality_of(:item_price).is_greater_than_or_equal_to(0) }
  end
end
