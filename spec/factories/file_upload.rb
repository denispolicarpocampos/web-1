FactoryBot.define do
  factory :file_upload do
    file { Rack::Test::UploadedFile.new(Rails.root.join('spec/support/example_input.tab'), 'application/octet-stream') }
    user
  end
end
