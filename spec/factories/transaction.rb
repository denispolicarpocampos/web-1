FactoryBot.define do
  factory :transaction do
    purchaser_name    { FFaker::Internet.user_name }
    item_description  { FFaker::AWS.product_description }
    item_price        { Random.rand(0...9999.99) }
    purchase_count    { Random.rand(1..9999) }
    merchant_address  { FFaker::Address.street_address }
    merchant_name     { FFaker::Internet.user_name }
    user
  end
end