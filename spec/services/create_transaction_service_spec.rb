require 'rails_helper'

describe CreateTransactionService do
  let(:user) { create(:user) }
  let(:upload) { build(:file_upload, user: user) }
  let(:file) { File.read(upload.file) }

  subject do
    described_class.new(file, user.id).call
  end

  context "result without errors" do
    it 'save on transaction value table' do
      expect{ subject }.to change{ Transaction.count }.by(file.lines.count - 1)
    end

    it 'file with just column title' do
      file_string = "purchaser name\titem description\titem price\tpurchase count\tmerchant address\tmerchant name\n"
      expect{described_class.new(file_string, user.id)}.to change {Transaction.count }.by(0)
    end

    it 'value is not numeric' do
      file_string = "purchaser name\titem description\titem price\tpurchase count\tmerchant address\tmerchant name\n
      João Silva\tR$10 off R$20 of food\t1dfadf0\t2\t987 Fake St\tBob's Pizza\n"
      expect{described_class.new(file_string, user.id)}.to change {Transaction.count }.by(0)
    end

    it 'value is smaller than 0' do
      file_string = "purchaser name\titem description\titem price\tpurchase count\tmerchant address\tmerchant name\n
      João Silva\tR$10 off R$20 of food\-5\t2\t987 Fake St\tBob's Pizza\n"
      expect{described_class.new(file_string, user.id)}.to change {Transaction.count }.by(0)
    end

    it 'user id is nil' do
      file_string = "purchaser name\titem description\titem price\tpurchase count\tmerchant address\tmerchant name\n
      João Silva\tR$10 off R$20 of food\-5\t2\t987 Fake St\tBob's Pizza\n"
      expect{described_class.new(file_string)}.to raise_error(ArgumentError)
    end

    it 'user id is empty' do
      expect{described_class.new(file, '')}.to change { Transaction.count }.by(0)
    end
  end
  
end