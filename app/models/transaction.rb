class Transaction < ApplicationRecord
  belongs_to :user

  validates :item_price, :purchaser_name, :item_description, :purchase_count, 
            :merchant_address, :user_id, :merchant_name, presence: true

  validates_numericality_of :item_price, greater_than_or_equal_to: 0
end
