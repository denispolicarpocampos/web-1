class AddTransactionJob < ApplicationJob # >
  queue_as :default

  def perform(file, user)
    CreateTransactionService.new(file, user).call
  end
end
