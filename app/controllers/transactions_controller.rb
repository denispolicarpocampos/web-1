class TransactionsController < ApplicationController
  def index
    transactions = current_user.transactions
    @transactions = transactions.paginate(page: params[:page], per_page: 15)
    @sum_values = transactions.sum(:item_price)
  end
end