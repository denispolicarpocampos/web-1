# encoding: utf-8

class CreateTransactionService

  def initialize(file, user)
    @file = file
    @user = user
  end

  def call
    create_tmp_file
  rescue StandardError => e
    raise ActiveRecord::Rollback
  end

  private

  def create_tmp_file
    ActiveRecord::Base.transaction do
      @file.each_line.with_index do |line, index|
        create_cnba(line) if index > 0
      end
    end
  end
  
  def create_cnba(line)
    line_tabs = line.chop.split("\t")
    Transaction.create!(
      purchaser_name: line_tabs[0],
      item_description: line_tabs[1],
      item_price: line_tabs[2],
      purchase_count: line_tabs[3],
      merchant_address: line_tabs[4],
      merchant_name: line_tabs[5],
      user_id: @user)
  end
end